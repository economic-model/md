> 五分钟 Markdown —— [连享会](https://www.lianxh.cn)

&emsp; 

>> 你可以点击右上角【Fork】按钮，把本项目 Fork 到你的码云主页下。定期强制同步，可以实现与本项目主页的同步更新。 你的码云主页下可以自建很多新项目。

&emsp;
- 五分钟 Markdown 视频：[新浪微博](https://weibo.com/tv/v/IzkIK5mHr?fid=1034:4484204327796746)；[短书直播](https://lianxh.duanshu.com/#/brief/video/1ad258af4a5b429d8edf2f20e843da2b)
- [Wikis](https://gitee.com/arlionn/md/wikis/Home) 里有详细的教程
- [幻灯片](https://gitee.com/arlionn/md/raw/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E4%BA%94%E5%88%86%E9%92%9FMarkdown-%E5%B9%BB%E7%81%AF%E7%89%87.pdf) [曲奇云盘版](https://quqi.gblhgk.com/s/880197/hUkcZsC5wH4eWAgF) 
- 用 Markdown 写一个幻灯片
  - 在谷歌浏览器中输入 [web.marp.app](web.marp.app)
  - 单击 | [「五分钟Markdown-幻灯片-原始文档」](https://gitee.com/arlionn/md/blob/master/%E4%BA%94%E5%88%86%E9%92%9FMarkdown-%E5%8E%9F%E5%A7%8B%E6%96%87%E6%A1%A3.do)，复制里面的文字。(或者，点击 [这里](https://gitee.com/arlionn/md/attach_files/352673/download) 下载)
  - 将上述文档的内容贴入 [web.marp.app](web.marp.app)，即可看到漂亮的幻灯片
  - 依次点击左上角的「**蓝色三角形图标**」&rarr;「**Print / Export to PDF**」 即可输出 PDF 格式的幻灯片。 
- Marp 幻灯片展示
  - 连玉君，[我的甲壳虫-直播课-幻灯片](https://gitee.com/arlionn/paper101/wikis/%E5%B9%BB%E7%81%AF%E7%89%87.md?sort_id=1985074)
  - 连玉君，[动态面板数据模型-幻灯片](https://gitee.com/arlionn/Live/tree/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E5%8A%A8%E6%80%81%E9%9D%A2%E6%9D%BF%E6%A8%A1%E5%9E%8B)


&emsp;


> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)


> [连享会 - Stata 暑期班](https://gitee.com/arlionn/PX)：直播，省去差旅费     
> &#x1F4C6; **时间：** 2020.7.28-8.7   
> &#x1F353; **主讲嘉宾**：连玉君 (中山大学) | 江艇 (中国人民大学)      
> &#x1F449; **课程主页**：<https://gitee.com/arlionn/PX> | [微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)   
> &#x1F34F; **助教招聘：** 15 名，免费听课，详见课程主页  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2020Stata暑期班海报竖版CL550.png)


&emsp;

## 相关课程

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  
> <img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[Stata暑期班](https://gitee.com/arlionn/PX)** | 连玉君 <br> 江艇| [线上直播 9 天](https://www.lianxh.cn/news/e87cdb5c116d0.html) <br> 2020.7.28-8.7 |
| **[效率分析-专题](https://gitee.com/arlionn/TE)** | 连玉君<br>鲁晓东<br>张&emsp;宁 | [视频-TFP-SFA-DEA](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，3天 |
| **文本分析/爬虫** | 游万海<br>司继春 | [视频-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，4天 |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">



 
&emsp; 

## 课程特色
- **深入浅出**：掌握最主流的实证分析方法，获取最新重现代码和文档
- **电子板书**：全程电子板书，课后分享；听课更专注，复习更高效。
- **电子讲义**：分享全套电子版课件，课程中的方法和代码都可以快速移植到自己的论文中。

&emsp;
 
&emsp;

> #### [连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)     
> **已上线**：可随时购买学习+全套课件，[课程主页](https://gitee.com/arlionn/TE) 已经放置板书和 FAQs   
> 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> [课程主页](https://gitee.com/arlionn/TE)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg) &ensp; <https://gitee.com/arlionn/TE>

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TE专题_连享会_lianxh.cn700.png "连享会-效率分析专题视频，三天课程，随时学")

&emsp; 



&emsp;

> #### [连享会 - 文本分析与爬虫 - 专题视频](https://www.lianxh.cn/news/88426b2faeea8.html)    
> 主讲嘉宾：司继春 || 游万海

![连享会-文本分析与爬虫-专题视频教程](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lanNew-文本分析-海报002.png "连享会-文本分析与爬虫-专题视频，四天课程，随随时学")




&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **你的颈椎还好吗？** 您将 [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 收藏起来，以便随时在电脑上查看往期推文。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：
  - `课程, 直播, 视频, 客服, 模型设定, 研究设计, `
  - `stata, plus，Profile, 手册, SJ, 外部命令, profile, mata, 绘图, 编程, 数据, 可视化`
  - `DID，RDD, PSM，IV，DID, DDD, 合成控制法，内生性, 事件研究` 
  - `交乘, 平方项, 缺失值, 离群值, 缩尾, R2, 乱码, 结果`
  - `Probit, Logit, tobit, MLE, GMM, DEA, Bootstrap, bs, MC, TFP`
  - `面板, 直击面板数据, 动态面板, VAR, 生存分析, 分位数`
  - `空间, 空间计量, 连老师, 直播, 爬虫, 文本, 正则, python`
  - `Markdown, Markdown幻灯片, marp, 工具, 软件, Sai2, gInk, Annotator, 手写批注`
  - `盈余管理, 特斯拉, 甲壳虫, 论文重现`
  - `易懂教程, 码云, 教程, 知乎`


---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")


> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

